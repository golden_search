# For licensing info see the included LICENSE file
#
# Josef Moudrik, <J dot Moudrik at standard google mail ending>, 2012

import math

from number import Number

def avg(l):
    assert len(l)
    return sum(l) / float(len(l))

class MethodAnalyzer:
    def __init__(self):
        self.method_data = {}
        self.method_avg_sdsq = {}

    def addMeasurement(self, m):
        assert m.method 
        l = self.method_data.get(m.method, [])
        l.append( m.computeStats() )
        self.method_data[m.method] = l

    def computeStats(self, verbose=False):
        for method, l in self.method_data.iteritems():
            avg_sdsq = avg( [ t[2] for t in l ] )
            min_sdsq = min( t[2] for t in l )
            max_sdsq = max( t[2] for t in l )

            self.method_avg_sdsq[method] = avg_sdsq
            if verbose:
                print method
                print "min =", min_sdsq
                print "avg =", avg_sdsq
                print "max =", max_sdsq

class M:
    def __init__(self, name, ms, method=None, units=None, precise=False):
        if units == None:
            units = {'m':1}

        if precise:
            assert len(ms) == 1

        self.name = name
        self.ms = map(float, ms)
        #self.ms = ms
        self.method = method
        self.units = units
        self.precise = precise

    def computeStats(self):
        n = len(self.ms)
        mean = avg(self.ms)
        # delime takhle kvuli nevychylenymu odhadu
        sdsq = sum((x - mean)**2 for x in self.ms) / ( (n - 1) if n > 1 else 1 )

        return (n, mean, sdsq)

    def toNumber(self, method_resolution=None, method_analyzer=None):
        if self.precise:
            return Number(self.ms[0], units=self.units, string=self.name)

        n, mean, sdsq = self.computeStats()
        if self.method:
            if method_analyzer:
                mavg = method_analyzer.method_avg_sdsq[self.method]
                if sdsq < mavg:
                    sdsq = mavg

            # the more measurements, the better
            sdsq /= n

            if method_resolution:
                # add the resolution of the method
                sdsq += method_resolution[self.method] ** 2

        return Number(mean, sdsq=sdsq, units=self.units, string=self.name)
