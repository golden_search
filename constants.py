# For licensing info see the included LICENSE file
#
# Josef Moudrik, <J dot Moudrik at standard google mail ending>, 2012

# -*- coding: utf-8 -*-

import math

from number import Number

def add_constant(n):
    CONSTANTS.append(n)

NAT_CONSTANTS = [
        Number(1.61803398874989, string='Golden Ratio'),
        Number(1/1.61803398874989, string='Golden Ratio Inverted'),
        Number(0.577215664901, string='Euler-Mascheroni'),
        Number(26000, string='Sun-Milky way center (in light years)'),
        Number(math.e, string='e'),
        Number(math.pi, string='Pi'),
        Number(math.sqrt(2), string=u'√2'),
        Number(math.log(2), string=u'ln(2)'),
        Number(147 * 10**9, units={'m':1}, string='Earth-Sun Perihelium distance'),
        Number(152 * 10**9, units={'m':1}, string='Earth-Sun Aphelium distance'),
        Number(384400 * 10**3, units={'m':1}, string='Earth-Moon distance'),
        Number(40075017, units={'m':1}, string='Earth Circumference Eq'),
        Number(40007860, units={'m':1}, string='Earth Circumference Pol'),
        Number(6378100, units={'m':1}, string='Earth Radius Eq'),
        Number(6356800, units={'m':1}, string='Earth Radius Pol'),
        Number(2.42, string='Earth Land:Sea Ratio'),
        #Number(299792458, units={'m':1, 's':-1}, string='Speed of Light')
    ]

CISLA = [
        Number(3.0/10, string='Cislo 3:10'),
        Number(6.0/10, string='Cislo 6:10'),
        Number(9.0/10, string='Cislo 9:10'),
        Number(10**3, string='Cislo 10^3'),
        Number(10**6, string='Cislo 10^6'),
        Number(10**9, string='Cislo 10^9'),
]
CONSTANTS = NAT_CONSTANTS
