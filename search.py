# For licensing info see the included LICENSE file
#
# Josef Moudrik, <J dot Moudrik at standard google mail ending>, 2012

from itertools import product, chain
import resource

from exc import IncompatibleUnits, MemExceeded

def search(base_numbers, ops, cc, max_round=None, mem_limit=2*1024**2):
    # Round 0
    bf = [base_numbers]
    # check if measurements do not mean anything directly
    for m in base_numbers:
        cc.checkCandidate(m, verbose=True)

    elem_count = 0
    final_round = max_round == 0
    round_count = 1
    try:
        while not final_round:
            final_round = max_round == round_count

            print
            print "ROUND %d started"%(round_count,)
            print
            ro = []
            try:
                for op in ops.get(round_count, ops.get('default', None)):
                    ar = op.getArity()
                    if ar >= 3:
                        raise NotImplementedError

                    last = bf[-1]
                    sofar = list(chain(*bf))

                    pr = [last] + [sofar]*(ar-1)
                    i1 = product(*pr)
                    i2 = ()

                    if not op.isCommutative() and ar == 2:
                        i2 = product(chain(*(bf[:-1])), last)

                    for t in chain(i1, i2):
                        if elem_count % 10000 == 0 and not final_round: 
                            mem_used = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
                            if mem_used > mem_limit:
                                print 
                                print "MEM EXCEEDED"
                                print "Limit %.1f MB, Used %.1f MB"%(mem_limit / 1024.0, mem_used / 1024.0)
                                print "FINAL ROUND FOLLOWS"
                                raise MemExceeded
                        elem_count += 1

                        try:
                            res = op(t)
                            # we do not need to add if we are in the final round
                            # we save a lot of memory this way (and may even be able to compute one more round therefore
                            if not final_round:
                                ro.append(res)
                            # register the result to see improving matches incrementaly
                            cc.checkCandidate(res, verbose=True)
                        except IncompatibleUnits:
                            pass
                        except ZeroDivisionError:
                            pass

            except MemExceeded:
                # set the limit so that we end in the next iteration
                max_round = round_count + 1

            bf.append(ro)
            print
            print "ROUND %d ended, searched %d combinations in total"%(round_count, elem_count)
            print

            round_count += 1
    except KeyboardInterrupt:
        pass

    print
    print
    print "END"
    print
    print "Best matches:"
    print
    # show what we have found
    cc.printBestMatches()
