# For licensing info see the included LICENSE file
#
# Josef Moudrik, <J dot Moudrik at standard google mail ending>, 2012

# -*- coding: utf-8 -*-

import math

from exc import IncompatibleUnits
import operators

def units_op(u1, u2, op):
    u = {}
    for k in u1.viewkeys() | u2.viewkeys():
        v1, v2 = u1.get(k, 0), u2.get(k, 0)
        v = op( v1, v2 )
        if v != 0:
            u[k] = v
    return u

def units_diff(u1, u2):
    return units_op(u1, u2, lambda x, y : x - y)

def units_join(u1, u2):
    return units_op(u1, u2, lambda x, y : x + y)

class Number:
    def __init__(self, value, sdsq=0, units=None, string='', parents=None):
        if units == None:
            units = {}

        self.value = float(value)
        self.sdsq = sdsq
        self.units = units
        self.string = string
        self.parents = parents

    def __add__(self, other):
        return operators.Plus()((self, other))

    def __sub__(self, other):
        return operators.Minus()((self, other))

    def __mul__(self, other):
        return operators.Mult()((self, other))

    def __rmul__(self, other):
        assert isinstance(other, int) or isinstance(other, float)

        return operators.Times(other)((self,))

    def __div__(self, other):
        return operators.Div()((self, other))

    def getDifference(self, other):
    # TODO lip??
        if self.getUnits() != other.getUnits():
            raise IncompatibleUnits

        assert self.sdsq == 0

        diff = abs(1.0 - other.getValue() / self.getValue())

        op = other.getSdPerc()
        if diff < op:
            return op

        return diff

    def getValue(self):
        return self.value

    def getSdSquare(self):
        return self.sdsq

    def getSd(self):
        return math.sqrt(self.getSdSquare())

    def getSdPerc(self):
        return self.getSd() / abs(self.getValue())

    def getUnits(self):
        return self.units

    def strUnits(self):
        l=[]
        for key in sorted(self.units.keys()):
            l.append(key)
            v = self.units[key]
            if v != 1:
                l.append( '^%d'%(v,))
        return ''.join(l)

    def longStr(self):
        return self.headStr() + " = " + str(self)

    def headStr(self):
        return u'%.4f ± %.4f %s'%(self.getValue(), self.getSd(), self.strUnits())

    def __str__(self):
        s = self.string
        if self.parents:
            s += "(%s)"%(', '.join(map(str, self.parents), ), )
        return s
