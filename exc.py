# For licensing info see the included LICENSE file
#
# Josef Moudrik, <J dot Moudrik at standard google mail ending>, 2012

class MemExceeded(Exception):
    pass

class IncompatibleUnits(Exception):
    pass
