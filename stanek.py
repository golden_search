# For licensing info see the included LICENSE file
#
# Josef Moudrik, <J dot Moudrik at standard google mail ending>, 2012

from correlation_checker import CorrelationChecker
from measurements import MethodAnalyzer
import operators
from constants import CONSTANTS
from search import search

from stanek_data import MEASURED_DATA, PRECISE_DATA, method_resolution

#
#       Data Preparation
#

# compute avg sdsq for each method
ma = MethodAnalyzer()
for m in MEASURED_DATA:
    ma.addMeasurement(m)
ma.computeStats(verbose=True)

# compute list of Numbers from measured data
# this takes into account:
#       1) method precision
#       2) number of samples

# Convert measurements to Numbers
MEASUREMENTS = map(lambda m: m.toNumber(method_resolution, ma), MEASURED_DATA ) # + PRECISE_DATA)
# Now I add some derived numbers, (like rectangle surface (a*b), once you have measured `a` and `b`.
md = dict( (n.string, n) for n in MEASUREMENTS )

obvod = 2 * md['Predek_Sirka'] + 2 * md['Bok_Sirka']
obvod.parents = None
obvod.string = 'Obvod_Stanku'
MEASUREMENTS.append(obvod)

for base in [ "Dvere", "Menu_Deska", "Stitek", "Klicova_Dirka", "Bok_O_2", "Predek_O_1", "Vnitrek_Okynka" ]:
    obsah = md[ base + "_Vyska" ] * md[ base + "_Sirka" ]
    obsah.parents = None
    obsah.string = base + '_Obsah'
    print md[ base + "_Vyska" ].longStr()
    print md[ base + "_Sirka" ].longStr()
    print obsah.longStr()
    MEASUREMENTS.append(obsah)

if __name__ == "__main__":
    #
    #       Define searching operators
    #


    MAX_ROUND = 1
    OPS_1 = [ ]
    #OPS_2 = [ ]
    #OPS_3 = [ ]

    OPS_1.append(operators.Div())

    #OPS_1.append(operators.Plus())
    #OPS_1.append(operators.Minus())
    #OPS_1.extend(operators.OPERATORS_ARITHMETIC)
#    for num in [1]: #, 10**10, 10**9, 10**6, 10**3]:#, 1/3.0, 1/6.0, 1/9.0]:
        #OPS_2.append(operators.Times(num))

    #for num in [10**10, 10**9, 10**6, 10**3]:#, 1/3.0, 1/6.0, 1/9.0]:
    #    OPS_2.append(operators.Times(num))
    #OPS_2.extend(reversed(operators.OPERATORS_ARITHMETIC))
    #OPS_2.append( operators.Div() )

    OPS = dict([
        (1, OPS_1)
        ])

    """
    OPS_1 = [ ]
    #OPS_1.append(operators.Div())
    OPS_1.append(operators.Plus())
    OPS_1.append(operators.Minus())
    #OPS_1.extend(perators.OPERATORS_ARITHMETIC)
    for num in [10**9, 10**6, 10**3 ]: # 1/3.0, 1/6.0, 1/9.0]:
        OPS_1.append(operators.Times(num))
    #OPS_2 = list(reversed(operators.OPERATORS_ARITHMETIC))
    OPS_2 = [ operators.Div() ]

    OPS = dict([
        (1, OPS_1),
        ('default', OPS_2)
        ])
    """
    #
    #       Search Targets
    #

    # we want to look for constants
    TARGETS = CorrelationChecker(CONSTANTS)

    #
    #       Run the search!
    #

    search( MEASUREMENTS, OPS, TARGETS, MAX_ROUND, mem_limit=1*1024**2)
