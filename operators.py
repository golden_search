# For licensing info see the included LICENSE file
#
# Josef Moudrik, <J dot Moudrik at standard google mail ending>, 2012

from math import sqrt

import number
from exc import IncompatibleUnits



OPERATORS_ALL=[]

OP_CLASSES=[]
OPERATORS_ARITHMETIC=[]

def register_operator(cl):
    OP_CLASSES.append(cl)
    return cl

class GenericOp(object):
    def __init__(self, arity, commutativity, name):
        self.arity = arity
        self.commutativity = commutativity
        self.name = name

    def __call__(self, args):
        assert len(args) == self.getArity()
        values = map(lambda n: n.getValue(), args)
        sdsqs = map(lambda n: n.getSdSquare(), args)
        units = map(lambda n: n.getUnits(), args)

        new_value, new_sdsq, new_units = self.computeNewNumber(values, sdsqs, units)
        return number.Number(new_value, sdsq=new_sdsq, string=self.name, units=new_units, parents=args)

    def computeNewNumber(self, values, sdsqs, units):
        raise NotImplementedError

    def getArity(self):
        return self.arity

    def isCommutative(self):
        return self.commutativity
#
#
#   Arithmetic Operators
#

@register_operator
class Plus(GenericOp):
    def __init__(self):
        super(Plus, self).__init__(2, True, "Plus")

    def computeNewNumber(self, (n1v, n2v), (n1sdsq, n2sdsq), (u1, u2)):
        if u1 != u2:
            raise IncompatibleUnits
        new_value = n1v + n2v
        new_sdsq = n1sdsq + n2sdsq
        return new_value, new_sdsq, u1

@register_operator
class Minus(GenericOp):
    def __init__(self):
        super(Minus, self).__init__(2, False, "Minus")

    def computeNewNumber(self, (n1v, n2v), (n1sdsq, n2sdsq), (u1, u2)):
        if u1 != u2:
            raise IncompatibleUnits
        new_value = n1v - n2v
        new_sdsq = n1sdsq + n2sdsq
        return new_value, new_sdsq, u1

@register_operator
class Mult(GenericOp):
    def __init__(self):
        super(Mult, self).__init__(2, True, "Mult")

    def computeNewNumber(self, (n1v, n2v), (n1sdsq, n2sdsq), (u1, u2)):
        new_value = n1v * n2v
        new_sdsq = n1v**2 * n2sdsq + n2v**2 * n1sdsq
        return new_value, new_sdsq, number.units_join(u1, u2)

@register_operator
class Div(GenericOp):
    def __init__(self):
        super(Div, self).__init__(2, False, "Div")

    def computeNewNumber(self, (n1v, n2v), (n1sdsq, n2sdsq), (u1, u2)):
        new_value = n1v / n2v
        new_sdsq = n2v**(-2) * n1sdsq + n1v**2 * n2v**(-4) * n2sdsq
        return new_value, new_sdsq, number.units_diff(u1, u2)

#
#       Numeric operators
#
# e.g. op(number) -> 2 * number
#

class Times(GenericOp):
    def __init__(self, num):
        super(Times, self).__init__(1, True, str(num))
        self.num=num

    def computeNewNumber(self, (n1v,), (n1sdsq, ), (u1,)):
        new_value = self.num * n1v
        new_sdsq = self.num**2 * n1sdsq
        return new_value, new_sdsq, u1

#
#       Unit Conversions
#

# TODO TODO TODO

#
#   All together
#

OPERATORS_ARITHMETIC = map(lambda x: x() , OP_CLASSES)

