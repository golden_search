# For licensing info see the included LICENSE file
#
# Josef Moudrik, <J dot Moudrik at standard google mail ending>, 2012

from exc import IncompatibleUnits

def encode_utf8(st):
	return unicode(st).encode('utf-8')

def myprint(*args):
    print encode_utf8(' '.join(map(unicode, args)))

class CorrelationChecker:
    def __init__(self, targets):
        self.targets = targets
        self.matchd = {}

    def checkCandidate(self, num, verbose=False, verbose_limit=0.1):
        for tid, t in enumerate(self.targets):
            try:
                diff = t.getDifference(num)
                diffsofar = self.matchd.get(tid, None)

                if diffsofar == None or diff < diffsofar[0]:
                    self.matchd[tid] = (diff, num)
                    if verbose and diff < verbose_limit:
                        self.printBestMatch(tid)
            except IncompatibleUnits:
                pass
            except ZeroDivisionError:
                pass

    def printBestMatches(self):
        tids = [ t[0] for t in sorted(self.matchd.items(), key=lambda t:t[1]) ]
        for tid in tids:
            self.printBestMatch(tid)

    def printBestMatch(self, tid):
        match = self.matchd.get(tid, None)
        if match:
            diff, num = match
            t = self.targets[tid]

            print 
            print "------ %.3f%%; "%(100*(1-diff),), diff
            myprint(t, "=", num)
            myprint(t.headStr())
            myprint(num.headStr())
