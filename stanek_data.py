# For licensing info see the included LICENSE file
#
# Josef Moudrik, <J dot Moudrik at standard google mail ending>, 2012

from measurements import M

# maximal possible precision of the measuring device
method_resolution = dict([
        ('Laser', 0.0005),
        ('Metr', 0.0005),
        ('Suplera', 0.000025)
    ])

# default units are meters
# else
# M( 'Speed_of_light', [ 300000000, 299000000 ], units={'m':1, 's':-1}, method='Laser')


MEASURED_DATA = [
#( '', [], method=''),
M( 'Predek_Sirka', [ 3, 2.99, 3.01 ], method='Laser'),
M( 'Predek_Vyska_Zeme_Deska', [ 0.897, 0.891 ], method='Laser'),
M( 'Vyska_k_Desce', [ 1.065, 1.065, 1.066, 1.066 ], method='Laser'),
M( 'Bok_Sirka', [2.0, 1.998], method='Laser'),
M( 'Dvere_Vyska', [1.931, 1.931], method='Laser' ),
M( 'Dvere_Sirka', [0.728, 0.727], method='Metr' ),
M( 'Okap_Prumer', [0.05315, 0.0533], method='Suplera'),
M( 'Deska_Sirka', [0.0302, 0.03], method='Suplera'),
M( 'Stupinek_Sirka', [0.906, 0.9], method='Metr'),
M( 'Stupinek_Vyska', [0.285, 0.28], method='Metr'),
M( 'Klika_Prumer', [0.04545, 0.04555], method='Suplera'),
M( 'Klicova_Dirka_Sirka', [0.00275, 0.0027], method='Suplera'),
M( 'Klicova_Dirka_Vyska', [0.01145, 0.01145], method='Suplera'),
M( 'Stitek_Sirka', [0.0398, 0.03965, 0.0397, 0.0397], method='Suplera'),
M( 'Stitek_Vyska', [0.233, 0.234, 0.233, 0.233], method='Metr'),
M( 'Menu_Deska_Sirka', [2.855, 2.866], method='Laser'),
M( 'Menu_Deska_Vyska', [0.273, 0.275], method='Laser'),
M( 'Menu_Deska_Obruben', [0.0384, 0.0382], method='Suplera'),
M( 'Od_Dveri_MFF', [6.753], method='Laser'),
M( 'Predek_O_1_Sirka', [0.249, 0.250, 0.251, 0.250], method='Laser'),
M( 'Predek_O_1_Vyska', [0.250, 0.248, 0.257, 0.253], method='Laser'),
#M( 'Predek_O_2_Sirka', [0.784, 0.783, 0.783, 0.783], method='Laser'),
#M( 'Predek_O_2_Vyska', [0.247, 0.247, 0.259, 0.253], method='Laser'),
M( 'Predek_O_3_Sirka', [0.384, 0.380, 0.378, 0.380], method='Laser'),
M( 'Predek_O_3_Vyska', [0.781, 0.781, 0.780, 0.781], method='Laser'),
#M( 'Predek_O_4_Sirka', [0.379, 0.377, 0.381, 0.380], method='Laser'),
#M( 'Predek_O_4_Vyska', [0.247, 0.249, 0.247, 0.247], method='Laser'),
M( 'Vnitrek_Okynka_Sirka', [0.3, 0.299], method='Laser'),
M( 'Vnitrek_Okynka_Vyska', [0.161, 0.161], method='Metr'),
M( 'Bok_O_1_Vyska', [1.377, 1.379, 1.382, 1.383], method='Laser'),
#M( 'Bok_O_1_Sirka', [0.432, 0.43, 0.428, 0.426], method='Laser'),
M( 'Bok_O_2_Sirka', [0.434, 0.433, 0.435, 0.435], method='Laser'),
M( 'Bok_O_2_Vyska', [0.382, 0.381, 0.380, 0.380], method='Laser')
]

PRECISE_DATA = [
M( 'Klobasa', [50], precise=True, units={}),
M( 'Maxi_Hot_Dog', [60], precise=True, units={}),
M( 'Hamburger', [30], precise=True, units={}),
M( 'Cheeseburger', [40], precise=True, units={}),
M( 'Hranolky', [35], precise=True, units={}),
M( 'Zero', [0], precise=True, units={})
# duplicitni:
#( 'Smazeny_Syr', [40], precise=True, units={}),
#( 'Langos', [50], precise=True, units={}),
#( 'Bramborak', [40], precise=True, units={})
]
